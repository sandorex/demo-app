package com.sandorex.cv.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class DBManager
{
    public static final int COOKIE_TIMEOUT              = 5;

    public static final int REQ_USER_INFO               = 1;
    public static final int REQ_EVENTS                  = 5;
    public static final int REQ_FRIEND_LIST             = 4;

    public static final int SUCCESS                     = 1;
    public static final int ERR                         = -1;
    public static final int ERR_INVALID_COOKIE          = -2;
    public static final int ERR_EXPIRED_COOKIE          = -27;
    public static final int ERR_INVALID_COOKIE_OWNER    = -23;
    public static final int ERR_DUPLICATE_LOGIN         = -30;
    public static final int ERR_DUPLICATE               = -66;
    public static final int ERR_INVALID_ARGUMENTS       = -7;
    public static final int ERR_INVALID_LOGIN           = -75;
    public static final int ERR_DB                      = -88; // ERR_MYSQL
    public static final int ERR_SERVER                  = -46;
    public static final int ERR_REGISTER                = -55;
    public static final int ERR_PARSING                 = -99;
    public static final int ERR_EMPTY_STRING            = -999;

    private final String URL = "http://192.168.2.100/alex/app/";
    private Context context;
    private RequestQueue rq;

    public DBManager(Context context)
    {
        this.context = context;
        this.rq = Volley.newRequestQueue(context);
    }

    public static abstract class HandleResponse
    {
        public void any(JSONObject obj) throws JSONException
        {
            if (obj.getInt("code") > 0)
                this.success(obj);
            else
                this.fail(obj);
        }

        public void exception(VolleyError error)
        {
            if (error != null) error.printStackTrace();
        }

        public void error()
        {
            JSONObject obj = new JSONObject();
            try
            {
                obj.put("depth", 0);
                obj.put("code", ERR_SERVER);
                this.fail(obj);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        public abstract void success(JSONObject obj) throws JSONException;
        public abstract void fail(JSONObject obj) throws JSONException;
    }

    public void add_event(String cookie, String name, String date, String[] friends, final HandleResponse hr)
    {
        String friends_combined = "";
        for (String s : friends)
        {
            friends_combined += " " + s;
        }
        if (friends_combined.startsWith(" "))
            friends_combined = friends_combined.substring(1);

        try
        {
            friends_combined = URLEncoder.encode(friends_combined, "UTF-8");
            name = URLEncoder.encode(name, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL + "event/add.php?cookie=" + cookie + "&name=" + name + "&date=" + date + "&friends=" + friends_combined, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            hr.any(response);
                        }
                        catch (JSONException e)
                        {
                            hr.error();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) // <---- HERE ERROR!
                    {
                        hr.exception(error);
                    }
                }
        );

        rq.add(request);
    }

    public void getUserInfo(String id, final HandleResponse hr)
    {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL + "user.php?id=" + id, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            hr.any(response);
                        }
                        catch (JSONException e)
                        {
                            hr.error();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        hr.exception(error);
                    }
                }
        );

        rq.add(request);
    }

    public void getAccountInfo(String cookie, final HandleResponse hr)
    {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL + "info.php?cookie=" + cookie + "&info=" + REQ_USER_INFO, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            hr.any(response);
                        }
                        catch (JSONException e)
                        {
                            hr.error();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        hr.exception(error);
                    }
                }
        );

        rq.add(request);
    }

    public void getEvents(String cookie, final HandleResponse hr)
    {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL + "info.php?cookie=" + cookie + "&info=" + REQ_EVENTS, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            hr.any(response);
                        }
                        catch (JSONException e)
                        {
                            hr.error();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        hr.exception(error);
                    }
                }
        );

        rq.add(request);
    }

    public void getFriends(String cookie, final HandleResponse hr)
    {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL + "info.php?cookie=" + cookie + "&info=" + REQ_FRIEND_LIST, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            hr.any(response);
                        }
                        catch (JSONException e)
                        {
                            hr.error();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        hr.exception(error);
                    }
                }
        );

        rq.add(request);
    }

    public void refreshSession(String cookie, final HandleResponse hr)
    {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL + "login.php?cookie=" + cookie, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            hr.any(response);
                        }
                        catch (JSONException e)
                        {
                            hr.error();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        hr.exception(error);
                    }
                }
        );

        rq.add(request);
    }

    public void createSession(String username, String password, final HandleResponse hr)
    {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL + "login.php?username=" + username + "&password=" + password, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            hr.any(response);
                        }
                        catch (JSONException e)
                        {
                            hr.error();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        hr.exception(error);
                    }
                }
        );

        rq.add(request);
    }

    public void createUser(String user, String password, String lorem, final HandleResponse hr)
    {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL + "register.php?username=" + user + "&password=" + password + "&lorem=" + lorem, null,
                new Response.Listener<JSONObject>()
                {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            if (response.getInt("code") == SUCCESS)
                            {

                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL + "register.php?cookie=" + response.getJSONObject("data").getString("cookie"), null, new Response.Listener<JSONObject>()
                                        {
                                            @Override
                                            public void onResponse(JSONObject response)
                                            {
                                                try
                                                {
                                                    hr.any(response);
                                                }
                                                catch (JSONException e)
                                                {
                                                    hr.error();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener()
                                        {
                                            @Override
                                            public void onErrorResponse(VolleyError error)
                                            {
                                                hr.exception(error);
                                            }
                                        }
                                    );
                                rq.add(request);
                            }
                            else
                                hr.fail(response);
                        }
                        catch (JSONException e)
                        {
                            hr.exception(null);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        hr.exception(error);
                    }
                }
        );

        rq.add(request);
    }
}
