package com.sandorex.cv.event;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sandorex.cv.MainActivity;
import com.sandorex.cv.R;
import com.sandorex.cv.utils.DBManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EventWelcomeActivity extends AppCompatActivity
{
    private LinearLayout ll_container;
    private Handler cookieHandler;
    private Runnable cookieRefresh;
    private DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_welcome);

        ll_container = (LinearLayout) findViewById(R.id.ll_ew_container);

        dbManager = new DBManager(this);
        cookieHandler = new Handler();

        cookieRefresh = new Runnable()
        {
            public void run()
            {
                dbManager.refreshSession(getIntent().getStringExtra("cookie"), new DBManager.HandleResponse()
                {
                    @Override
                    public void success(JSONObject obj) throws JSONException
                    {
                    }

                    @Override
                    public void fail(JSONObject obj) throws JSONException
                    {
                        int code = obj.getInt("code");

                        if (code == DBManager.ERR_EXPIRED_COOKIE)
                        {
                            Toast.makeText(EventWelcomeActivity.this, "Session has expired please relogin", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                            startActivity(intent);
                        }
                        else
                            Toast.makeText(EventWelcomeActivity.this, "Error (" + obj.getInt("depth") + "." + code + ")", Toast.LENGTH_SHORT).show();
                    }
                });
                cookieHandler.postDelayed(this, DBManager.COOKIE_TIMEOUT * 1000);
            }
        };

        dbManager.getEvents(getIntent().getStringExtra("cookie"), new DBManager.HandleResponse()
        {
            @Override
            public void success(JSONObject obj) throws JSONException
            {
                JSONArray objJSONArray = obj.getJSONArray("data");
                for (int i = 0; i < objJSONArray.length(); i++)
                {
                    JSONObject o = ((JSONObject) objJSONArray.get(i));
                    showEvent(o.getString("name"), o.getString("date"), o.getString("friends_invited"), o.getString("date_created"));
                }
            }

            @Override
            public void fail(JSONObject obj) throws JSONException
            {
                Toast.makeText(EventWelcomeActivity.this, "Error (" + obj.getInt("code") + ")", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showEvent(String name, String date, String invited, String date_created)
    {
        final LayoutInflater layoutInflater = LayoutInflater.from(this);
        final RelativeLayout layout = (RelativeLayout) layoutInflater.inflate(R.layout.activity_event_welcome_event_view, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        ((TextView) layout.findViewById(R.id.txt_ewe_name)).setText(name);
        ((TextView) layout.findViewById(R.id.txt_ewe_date)).setText(date);
        ((TextView) layout.findViewById(R.id.txt_ewe_date_created)).setText("(Created on " + date_created + ")");

        params.setMargins(0, 0, 0, 20);
        layout.setLayoutParams(params);
        ll_container.addView(layout);
    }

    public void create_event_event(View view)
    {
        Intent intent = new Intent(getBaseContext(), EventActivity.class);
        intent.putExtra("cookie", getIntent().getStringExtra("cookie"));
        startActivity(intent);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        this.cookieHandler.removeCallbacks(cookieRefresh);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        this.cookieHandler.post(cookieRefresh);
    }
}
