package com.sandorex.cv.event;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sandorex.cv.MainActivity;
import com.sandorex.cv.R;
import com.sandorex.cv.WelcomeActivity;
import com.sandorex.cv.utils.DBManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class EventActivity extends AppCompatActivity
{
    private Handler cookieHandler;
    private Runnable cookieRefresh;
    private DBManager dbManager;
    private EditText etxt_date, etxt_name;
    private LinearLayout ll_container;
    private HashMap<String, String> friends_map = new HashMap<>();

    private final Calendar cal1 = Calendar.getInstance();
    private final Calendar cal2 = cal1.getInstance();
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        etxt_name = (EditText) findViewById(R.id.etxt_e_name);
        etxt_date = (EditText) findViewById(R.id.etxt_e_date);
        ll_container = (LinearLayout) findViewById(R.id.ll_e_container);

        dbManager = new DBManager(this);
        cookieHandler = new Handler();

        cookieRefresh = new Runnable()
        {
            public void run()
            {
                dbManager.refreshSession(getIntent().getStringExtra("cookie"), new DBManager.HandleResponse()
                {
                    @Override
                    public void success(JSONObject obj) throws JSONException
                    {
                    }

                    @Override
                    public void fail(JSONObject obj) throws JSONException
                    {
                        int code = obj.getInt("code");

                        if (code == DBManager.ERR_EXPIRED_COOKIE)
                        {
                            Toast.makeText(EventActivity.this, "Session has expired please relogin", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                            finish();
                            startActivity(intent);
                        }
                        else
                            Toast.makeText(EventActivity.this, "Error (" + obj.getInt("depth") + "." + code + ")", Toast.LENGTH_SHORT).show();
                    }
                });
                cookieHandler.postDelayed(this, DBManager.COOKIE_TIMEOUT * 1000);
            }
        };

        this.etxt_date.setText(simpleDateFormat.format(cal1.getTime()));

        dbManager.getFriends(getIntent().getStringExtra("cookie"), new DBManager.HandleResponse()
        {
            @Override
            public void success(JSONObject obj) throws JSONException
            {
                JSONArray objJSONArray = obj.getJSONArray("data");
                for (int i = 0; i < objJSONArray.length(); i++)
                {
                    showFriend(((JSONObject) objJSONArray.get(i)).getString("id"));
                }
            }

            @Override
            public void fail(JSONObject obj) throws JSONException
            {
                if (obj.getInt("code") == DBManager.ERR_DB) return;
                Toast.makeText(EventActivity.this, "Error (" + obj.getInt("code") + ") while getting friend list", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showFriend(final String id)
    {
        final LayoutInflater layoutInflater = LayoutInflater.from(this);
        final LinearLayout layout = (LinearLayout) layoutInflater.inflate(R.layout.activity_event_friend_view, null);

        dbManager.getUserInfo(id, new DBManager.HandleResponse()
        {
            @Override
            public void success(JSONObject obj) throws JSONException
            {
                JSONObject obj_data = obj.getJSONObject("data");
                final String id = obj_data.getString("id");
                final String username = obj_data.getString("username");

                ((TextView) layout.findViewById(R.id.txt_ef_username)).setText("[" + id + "] " + username);
                layout.findViewById(R.id.ibtn_ef_add).setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (friends_map.containsKey(id))
                        {
                            Toast.makeText(EventActivity.this, "Removed \"" + username + "\"", Toast.LENGTH_SHORT).show();
                            ((ImageButton) v).setImageResource(R.drawable.add_icon);
                            friends_map.remove(id);
                        }
                        else
                        {
                            Toast.makeText(EventActivity.this, "Added \"" + username + "\"", Toast.LENGTH_SHORT).show();
                            ((ImageButton) v).setImageResource(R.drawable.remove_icon);
                            friends_map.put(id, "1");
                        }
                    }
                });
                ll_container.addView(layout);
            }

            @Override
            public void fail(JSONObject obj) throws JSONException
            {
                if (obj.getInt("code") == DBManager.ERR_DB)
                {
                    Toast.makeText(EventActivity.this, "Invalid friend \"" + id + "\"", Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(EventActivity.this, "Error (" + obj.getInt("code") + ") while getting user info", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void calendar_show_event(View view)
    {

        new DatePickerDialog(EventActivity.this, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                cal2.set(Calendar.YEAR, year);
                cal2.set(Calendar.MONTH, monthOfYear);
                cal2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etxt_date.setText(simpleDateFormat.format(cal2.getTime()));
            }

        }, cal1.get(Calendar.YEAR), cal1.get(Calendar.MONTH), cal1.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void add_event_event(View view)
    {
        if (etxt_name.getText().toString().isEmpty() || etxt_date.getText().toString().isEmpty())
        {
            Toast.makeText(this, "These fields cannot be empty!", Toast.LENGTH_SHORT).show();
            return;
        }

        dbManager.add_event(getIntent().getStringExtra("cookie"), etxt_name.getText().toString(), etxt_date.getText().toString(), friends_map.keySet().toArray(new String[friends_map.keySet().size()]), new DBManager.HandleResponse()
        {
            @Override
            public void success(JSONObject obj) throws JSONException
            {
                Toast.makeText(EventActivity.this, "Successfully added event", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getBaseContext(), EventWelcomeActivity.class);
                intent.putExtra("cookie", getIntent().getStringExtra("cookie"));
                finish();
                startActivity(intent);
            }

            @Override
            public void fail(JSONObject obj) throws JSONException
            {
                Toast.makeText(EventActivity.this, "Error (" + obj.getInt("depth") + "." + obj.getInt("code") + ")", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void cancel_event(View view)
    {
        Intent intent = new Intent(getBaseContext(), EventWelcomeActivity.class);
        intent.putExtra("cookie", getIntent().getStringExtra("cookie"));
        finish();
        startActivity(intent);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        this.cookieHandler.removeCallbacks(cookieRefresh);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        this.cookieHandler.post(cookieRefresh);
    }
}
