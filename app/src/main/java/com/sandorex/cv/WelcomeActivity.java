package com.sandorex.cv;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.sandorex.cv.event.EventWelcomeActivity;
import com.sandorex.cv.utils.DBManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class WelcomeActivity extends AppCompatActivity
{
    private Handler cookieHandler;
    private Runnable cookieRefresh;
    private DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        dbManager = new DBManager(this);
        cookieHandler = new Handler();

        cookieRefresh = new Runnable()
        {
            public void run()
            {
                dbManager.refreshSession(getIntent().getStringExtra("cookie"), new DBManager.HandleResponse()
                {
                    @Override
                    public void success(JSONObject obj) throws JSONException
                    {
                    }

                    @Override
                    public void fail(JSONObject obj) throws JSONException
                    {
                        int code = obj.getInt("code");

                        if (code == DBManager.ERR_EXPIRED_COOKIE && ((obj.has("data") && !obj.getJSONObject("data").has("changed")) || !obj.has("data")))
                        {
                            Toast.makeText(WelcomeActivity.this, "Session has expired please relogin", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                            startActivity(intent);
                        }
                        else
                            Toast.makeText(WelcomeActivity.this, "Error (" + obj.getInt("depth") + "." + code + ")", Toast.LENGTH_SHORT).show();
                    }
                });
                cookieHandler.postDelayed(this, DBManager.COOKIE_TIMEOUT * 1000);
            }
        };
    }

    public void calendar_event(View view)
    {
        Intent intent = new Intent(getBaseContext(), EventWelcomeActivity.class);
        intent.putExtra("cookie", getIntent().getStringExtra("cookie"));
        startActivity(intent);
    }

    public void camera_event(View view)
    {

    }

    @Override
    protected void onPause()
    {
        super.onPause();
        this.cookieHandler.removeCallbacks(cookieRefresh);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        this.cookieHandler.post(cookieRefresh);
    }
}
