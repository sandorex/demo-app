package com.sandorex.cv;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sandorex.cv.utils.DBManager;

import org.json.JSONException;
import org.json.JSONObject;

import static com.sandorex.cv.utils.DBManager.ERR_DUPLICATE_LOGIN;
import static com.sandorex.cv.utils.DBManager.ERR_INVALID_LOGIN;

public class MainActivity extends AppCompatActivity
{
    private DBManager dbManager;
    private EditText etxt_username, etxt_password;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbManager = new DBManager(this);
        etxt_username = (EditText) findViewById(R.id.etxt_username);
        etxt_password = (EditText) findViewById(R.id.etxt_password);
    }

    public void event_login(View view)
    {
        final String username, password;
        username = etxt_username.getText().toString();
        password = etxt_password.getText().toString();

        if (username.isEmpty() || password.isEmpty())
        {
            Toast.makeText(this, "These fields cannot be empty!", Toast.LENGTH_SHORT).show();
            return;
        }

        dbManager.createSession(username, password, new DBManager.HandleResponse()
        {
            @Override
            public void success(JSONObject obj) throws JSONException
            {
                Toast.makeText(MainActivity.this, "Welcome " + username, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getBaseContext(), WelcomeActivity.class);
                intent.putExtra("cookie", obj.getJSONObject("data").getString("cookie"));
                startActivity(intent);
            }

            @Override
            public void fail(JSONObject obj) throws JSONException
            {
                int code = obj.getInt("code");

                if (code == ERR_INVALID_LOGIN)
                    Toast.makeText(MainActivity.this, "Incorrect password or username", Toast.LENGTH_SHORT).show();
                else if (code == ERR_DUPLICATE_LOGIN)
                    Toast.makeText(MainActivity.this, "You are already logged in on another device", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(MainActivity.this, "Error (" + obj.getInt("depth") + "." + code + ")", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void event_register(View view)
    {
        Intent intent = new Intent(getBaseContext(), RegisterActivity.class);
        startActivity(intent);
    }
}
