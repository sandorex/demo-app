package com.sandorex.cv;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sandorex.cv.utils.DBManager;

import org.json.JSONException;
import org.json.JSONObject;

import static com.sandorex.cv.utils.DBManager.ERR_DUPLICATE;
import static com.sandorex.cv.utils.DBManager.ERR_REGISTER;
import static com.sandorex.cv.utils.DBManager.ERR_SERVER;
import static com.sandorex.cv.utils.DBManager.SUCCESS;

public class RegisterActivity extends AppCompatActivity
{
    private DBManager dbManager;
    private EditText etxt_username, etxt_password, etxt_lorem;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        dbManager = new DBManager(this);
        etxt_username = (EditText) findViewById(R.id.etxt_r_username);
        etxt_password = (EditText) findViewById(R.id.etxt_r_password);
        etxt_lorem = (EditText) findViewById(R.id.etxt_r_lorem);
    }

    public void register_event(View view)
    {
        final String username, password, lorem;
        username = etxt_username.getText().toString();
        password = etxt_password.getText().toString();
        lorem = etxt_lorem.getText().toString();

        if (username.isEmpty() || password.isEmpty() || lorem.isEmpty())
        {
            Toast.makeText(this, "These fields cannot be empty!", Toast.LENGTH_SHORT).show();
            return;
        }

        dbManager.createUser(username, password, lorem, new DBManager.HandleResponse()
        {
            @Override
            public void success(JSONObject obj) throws JSONException
            {
                Toast.makeText(RegisterActivity.this, "Please login to access your account", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
            }

            @Override
            public void fail(JSONObject obj) throws JSONException
            {
                int code = obj.getInt("code");

                switch(code)
                {
                    case SUCCESS:
                        Toast.makeText(RegisterActivity.this, "Please login to access to your account", Toast.LENGTH_SHORT).show();
                        break;
                    case ERR_SERVER:
                        Toast.makeText(RegisterActivity.this, "Error connecting to database", Toast.LENGTH_SHORT).show();
                        return;
                    case ERR_DUPLICATE:
                        Toast.makeText(RegisterActivity.this, "Account already exists with username \"" + username + "\"", Toast.LENGTH_SHORT).show();
                        return;
                    case ERR_REGISTER:
                        Toast.makeText(RegisterActivity.this, "Error making account with username \"" + username + "\"", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(RegisterActivity.this, "Error (" + obj.getInt("depth") + "." + code + ")", Toast.LENGTH_SHORT).show();
                        return;
                }

                Intent intent = new Intent(getBaseContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}
